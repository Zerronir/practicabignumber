import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;

class BigNumber {

    private String cadena;
    private BigNumber numeroGrande;
    // Constructor 1
    public BigNumber(String s) {
        this.cadena = s;
    }

    // Constructor 2
    public BigNumber(BigNumber b) {
        this.numeroGrande = b;
    }

    // Suma
    BigNumber add(BigNumber other) {

        String numero1 = this.cadena.replaceFirst("^0+(?!$)", "");
        String numero2 = other.cadena.replaceFirst("^0+(?!$)", "");

        //Si las dos strings son iguales las sumamos
        if(numero1.length() == numero2.length()){
            int num1 = Integer.parseInt(numero1);
            int num2 = Integer.parseInt(numero2);
            //Sumamos las dos cadenas
            int suma = num1+num2;

            //Transformamos el resultado de la suma a tipo BigNumber
            BigNumber resultat = new BigNumber(String.valueOf(suma));

            //Devolvemos el resultado
            return resultat;
        }

        //Inicializamos una variable para el acumulado de la suma
        if(numero1.length() < numero2.length()){
            //Creamos una string nueva para meterle el resutlado.
            StringBuilder newNum1 = new StringBuilder();

            for (int i = numero1.length(); i < numero2.length(); i++) {
                //Añadimos ceros a la string que hemos creado antes
                newNum1.append(0);
            }

            //Devolvemos una nueva string con los ceros más el número original
            String resNum1 = newNum1+numero1;


            StringBuilder resultatSuma = new StringBuilder();
            int acumuluado = 0;
            String sumaFinal = "";

            for (int i = 0; i < resNum1.length(); i++) {
                //Invertim les strings per sumar de dreta a esquerra
                StringBuilder reverse1 = new StringBuilder(resNum1);
                StringBuilder reverse2 = new StringBuilder(numero2);

                String reversedNum1 = reverse1.reverse().toString();
                String reversedNum2 = reverse2.reverse().toString();

                int appending = 0;
                int num1 = Integer.parseInt(String.valueOf(reversedNum1.charAt(i)));
                int num2 = Integer.parseInt(String.valueOf(reversedNum2.charAt(i)));

                int digitAdd = num1 + num2 + acumuluado;

                acumuluado = 0;

                //Si la suma és més de 9 restarem 10 i afegirem 1 al acumulat
                if(digitAdd > 9){
                    appending = digitAdd - 10;
                    acumuluado++;
                }else{
                    //En cas contrari ho sumarem al resultat que ja tenim
                    appending = digitAdd + acumuluado;
                }

                //Afegim el resultat a la cadena
                resultatSuma.append(appending);

            }

            //Invertimos el resultado para poder devolverlo después
            StringBuilder suma = new StringBuilder(resultatSuma.reverse());
            sumaFinal = suma.toString();

            //Transformamos el resultado en tipo BigNumber y creamos el return
            BigNumber resultat = new BigNumber(sumaFinal);
            return resultat;

        }else{
            String reversed1 = "";
            String reversed2 = "";
            StringBuilder newNum2 = new StringBuilder();
            String resNum2 = "";
            int acumulado = 0;
            int appending = 0;
            int digitAdd = 0;
            int num1 = 0;
            int num2 = 0;
            //Repetim les passes per al cas de que la longitud de numero2 sigui menor a la de número 1
            for (int i = numero2.length(); i < numero1.length(); i++) {
                newNum2.append(0);
            }

            resNum2 = newNum2+numero2;
            StringBuilder resultatSuma = new StringBuilder();

            for (int i = 0; i < numero1.length(); i++) {

                StringBuilder reverse1 = new StringBuilder(resNum2);
                StringBuilder reverse2 = new StringBuilder(numero1);

                reversed1 = reverse1.reverse().toString();
                reversed2 = reverse2.reverse().toString();

                num1 = Integer.parseInt(reversed1.valueOf(reversed1.charAt(i)));
                num2 = Integer.parseInt(reversed2.valueOf(reversed2.charAt(i)));

                digitAdd = num1 + num2 + acumulado;
                acumulado = 0;

                if(digitAdd > 9){
                    appending = digitAdd - 10;
                    acumulado++;
                }else{
                    appending = digitAdd + acumulado;
                }
                resultatSuma.append(appending);

            }
            String sumaFinal = "";
            //Invertimos el resultado para poder devolverlo después
            StringBuilder suma = new StringBuilder(resultatSuma.reverse());
            sumaFinal = suma.toString();

            //Transformamos el resultado en tipo BigNumber y creamos el return
            BigNumber resultat = new BigNumber(sumaFinal);
            return resultat;
        }
    }

    // Resta
    BigNumber sub(BigNumber other) {
        String numero1 = this.cadena;
        String numero2 = other.cadena;
        int num1 = 0;
        int num2 = 0;
        int resultatRestaInt = 0;
        StringBuilder addZeros = new StringBuilder(numero2);

        //Comprovam quin número té més longitud i feim que les cadenes siguin iguals
        if(numero1.length() < numero2.length()){
            addZeros = new StringBuilder();
            String resZeros = "";

            for (int i = numero1.length(); i < numero2.length(); i++) {
                addZeros.append(0);
            }

            resZeros = addZeros+numero1;
            int acumulado = 0;
            int appending = 0;
            StringBuilder resResta = new StringBuilder();

            for (int i = 0; i < numero1.length(); i++) {
                //Invertim les cadenes per operar de dreta a esquerra
                StringBuilder reverse1 = new StringBuilder(resZeros);
                StringBuilder reverse2 = new StringBuilder(numero2);

                String reversed1 = reverse1.reverse().toString();
                String reversed2 = reverse2.reverse().toString();

                //Les convertim a integer
                num1 = Integer.parseInt(reversed1.valueOf(reversed1.charAt(i)));
                num2 = Integer.parseInt(reversed2.valueOf(reversed2.charAt(i)));

                //Operam igual que a la suma pero de manera inversa
                int res = num1 - num2 - acumulado;
                acumulado = 0;
                if(res < 0){
                    appending = res + 10;
                    acumulado++;
                }else{
                    appending = res - acumulado;
                }

                resResta.append(appending);

            }
            String resultatFinal = "";

            StringBuilder resu = new StringBuilder(resResta.reverse());
            resultatFinal = resu.toString();

            //Retornam el resultat.
            BigNumber resultat = new BigNumber(resultatFinal);
            return resultat;

            //Comprobamos si las dos cadenas son igual de largas y operamos con ellas
        }else if(numero1.length() == numero2.length()){

            num1 = Integer.parseInt(numero1);
            num2 = Integer.parseInt(numero2);
            resultatRestaInt = num1 - num2;

            //Creamos un objeto del tipo BigNumber para almacenar el resultado de la resta
            BigNumber resultat = new BigNumber(String.valueOf(resultatRestaInt));
            return resultat;


        }else{

            addZeros = new StringBuilder();
            String resZeros = "";

            //Feim que les dues cadenes tenguin la mateixa longitud afegint 0s a la més curta.
            for (int i = numero2.length(); i < numero1.length(); i++) {
                addZeros.append(0);
            }
            //Cream la string amb els 0s i la cadena original
            resZeros = addZeros+numero2;
            int acumulado = 0;
            int appending = 0;
            StringBuilder resResta = new StringBuilder();

            for (int i = 0; i < numero1.length(); i++) {
                //Invertim les dues cadenes per operar de dreta a esuqerra
                StringBuilder reverse1 = new StringBuilder(numero1);
                StringBuilder reverse2 = new StringBuilder(resZeros);

                String reversed1 = reverse1.reverse().toString();
                String reversed2 = reverse2.reverse().toString();

                //Les passam a integer
                num1 = Integer.parseInt(reversed1.valueOf(reversed1.charAt(i)));
                num2 = Integer.parseInt(reversed2.valueOf(reversed2.charAt(i)));

                //Feim les mateixes passes que a la suma pero a la inversa
                int res = num1 - num2 - acumulado;
                acumulado = 0;
                if(res < 0){
                    appending = res + 10;
                    acumulado++;
                }else{
                    appending = res - acumulado;
                }

                resResta.append(appending);

            }
            String resultatFinal = "";

            StringBuilder resu = new StringBuilder(resResta.reverse());
            resultatFinal = resu.toString();


            BigNumber resultat = new BigNumber(resultatFinal);
            return resultat;
        }



    }

    // Multiplica
    BigNumber mult(BigNumber other) {
        String n1 = this.cadena;
        String n2 = other.cadena;
        String[] mult1 = new String[]{this.cadena};
        String[] mult2 = new String[]{other.cadena};

        if(n1.length() < 10){
            int numMult1 = Integer.parseInt(n1);
            int numMult2 = Integer.parseInt(n2);
            int multRes = 0;

            multRes = numMult1 * numMult2;
            return new BigNumber(String.valueOf(multRes));

        }else{

            Long num1 = Long.parseLong(this.cadena);
            Long num2 = Long.parseLong(other.cadena);
            Long multRes = 0l;

            multRes = Math.multiplyExact(num1, num2);
            Long rs = multRes;
            return new BigNumber(String.valueOf(multRes));

        }
    }

    // Divideix
    BigNumber div(BigNumber other) {return null; }

    // Arrel quadrada
    BigNumber sqrt() {

        String numero1 = this.cadena;

        if(numero1.length() < 10){

            double num = Double.parseDouble(numero1);

            double arrel = Math.sqrt(num);

            int rs = (int)arrel;
            this.cadena = Integer.toString(rs);


            String returner = this.cadena;

            return new BigNumber(returner);

        }else if(numero1.length() <= 21){

            double num = Double.parseDouble(numero1);
            double arrel = Math.sqrt(num);

            Long raiz = new Double(arrel).longValue();

            this.cadena = raiz.toString();

            String rt = this.cadena;

            return new BigNumber(rt);
        }else{
            double num = Double.parseDouble(numero1);
            double arrel = Math.sqrt(num);

            String rz = Double.toString(arrel);

            double[] raizz = new double[rz.length()];
            for(int i = 0; i < rz.length(); i++){
                raizz[i]= rz.charAt(i);
            }
            StringBuilder aux = new StringBuilder();
            StringBuilder rs = new StringBuilder();
            String aux2 = "";
            String aux3 = "";
            StringBuilder test = new StringBuilder();

            for (int i = 0; i < numero1.length(); i++) {
                aux.append(numero1.substring(i, i+1));
                aux2 = aux.toString();
                test.append(aux2);
                aux3 = test.toString();
                if(aux2.length() == 2) {
                    double numToSquare = Integer.parseInt(aux2);
                    double sq = Math.sqrt(numToSquare);
                    int rsq = (int) sq;

                    rs.append(rsq);
                    aux.delete(i, i + 2);
                    aux2 = "";
                    aux3 = "";

                }
            }

            String resultat = Arrays.deepToString(new String[]{Arrays.toString(raizz)});

            return new BigNumber(resultat);
        }

    }

    // Potència
    BigNumber power(int n) {

        BigNumber result = new BigNumber(this.cadena);
        String numero = this.cadena;

        if(numero.length() < 10){
            if(n < 10){
                int num = Integer.parseInt(numero);

                int re = (int) Math.pow(num, n);
                return new BigNumber(String.valueOf(re));

            }else{
                    BigInteger b1 = new BigInteger(numero);
                    b1 = b1.pow(n);
                    return new BigNumber(String.valueOf(b1));
                }

            }else{

                BigInteger b1 = new BigInteger(numero);

                b1 = b1.pow(n);

                return new BigNumber(String.valueOf(b1));
            }

        }

    // Factorial
    BigNumber factorial() {

        long fct = 1l;
        long num = Long.parseLong(this.cadena);

        for (long i = num; i > 0 ; i--) {
            fct = fct * i;
        }

        String rs = Long.toString(fct);
        System.out.println(rs);

        return new BigNumber(rs);
    }

    // MCD. Torna el Màxim comú divisor
    BigNumber mcd(BigNumber other) {

        String num1 = this.cadena;
        String num2 = other.cadena;

        //Transformam les strings en números
        Long n1 = Long.parseLong(num1);
        Long n2 = Long.parseLong(num2);

        //Cream una funció que està definida més abaix i la empleam aquí.
        Long mcd = obtainMCD(n1, n2);

        //Cream un nou objecte BigNumber per tornar el resultat.
        return new BigNumber(Long.toString(mcd));
    }

    // Compara dos BigNumber. Torna 0 si són iguals, -1
    // si el primer és menor i torna 1 si el segon és menor
    public int compareTo(BigNumber other) {

        String numero1 = this.cadena.replaceFirst("^0+(?!$)", "");
        String numero2 = other.cadena.replaceFirst("^0+(?!$)", "");

        //Si las dos cadenas son iguales devolvemos 0
        if(numero1.equals(numero2)){
            return 0;
        }

        /*
            Comparamos que la longitud de las dos Strings sea igual
            En caso de que lo sean pasamos a comparar los caracteres de la misma
         */
        if(numero1.length() == numero2.length()){
            for (int i = 0; i < numero1.length(); i++) {

                //Si el caracter de i dentro de la primera cadena es menor que al valor de la misma posición de la segunda devolvemos 1
                if(numero1.charAt(i) < numero2.charAt(i)){
                    return -1;

                //Hacemos la inversa del paso anterior
                }else if(numero2.charAt(i) < numero1.charAt(i)){
                    return 1;
                }
            }
        //Si la longitud de la primera cadena es inferior a la de la segunda devolvemos -1
        }else if(numero1.length() < numero2.length()){
            return -1;

        //En caso contrario devolvemos 1
        }else {
            return 1;
        }

        return 30;
    }

    // Torna un String representant el número
    public String toString() {
        return null;
    }

    // Mira si dos objectes BigNumber són iguals
    public boolean equals(Object other) {
        BigNumber numGran = ((BigNumber) other);

        //Eliminam els 0s al començament de la string
        String newString = numGran.cadena.replaceFirst("^0+(?!$)", "");

        //Retornam el resultat, en cas de que siguin iguals serà true i en cas contrari serà false
        return this.cadena.equals(newString);
    }

    static Long obtainMCD(Long a, Long b) {

        //Si el segon número es 0, retornam el primer com a MCD
        if(b==0) {
            return a;
        }else {
            //En cas de que b sigui > 0 calcularem el mcd entre b i el mòdul de a%b. Que serà el resultat
            return obtainMCD(b, a % b);
        }
    }

}