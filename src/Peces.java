
public class Peces {
    public static void main(String[] args) {
        Peix p1 = new Peix();
        p1.setNom("Tonyina");
        Peix p2 = new Peix();
        p2.setNom("Lluç");
        Peix p3 = new Peix(p2);
        Peix p4 = p2;

        System.out.println("--");
        System.out.println(p1.getNom());
        System.out.println(p2.getNom());
        System.out.println(p3.getNom());
        System.out.println(p4.getNom());

        p2.setNom("Calamar");

        System.out.println("--");
        System.out.println(p1.getNom());
        System.out.println(p2.getNom());
        System.out.println(p3.getNom());
        System.out.println(p4.getNom());

        System.out.println("--");
        System.out.println(p1.equals(p2));
        System.out.println(p2.equals(p4));

        System.out.println("--");
        System.out.println("Nombre de peixos creats: " + Peix.count);
    }
}

class Peix {
    static int count;
    String nom = "Peix desconegut";

    Peix() {
        count++;
    }

    Peix(Peix p) {
        this.nom = p.nom;
        count++;
    }

    void setNom(String nom) {
        this.nom = nom;
    }

    String getNom() {
        return this.nom;
    }

    public boolean equals(Peix p) {
        return this.nom.equals(p.nom);
    }
}
